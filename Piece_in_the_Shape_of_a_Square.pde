/**
 * Piece in the Shape of a Square.
 *
 * @author @deconbatch
 * @version 0.1
 * Processing 3.2.1
 * created 2019.02.03
 * updated 2019.09.01 : makes checker pattern in color
 */

void setup() {

  size(720, 720);
  colorMode(HSB, 360.0, 100.0, 100.0, 100.0);
  smooth();
  noLoop();

}

void draw() {

  int   lastFrameCnt = 24;
  int   frameMax     = 24 * 6;
  float baseHue      = random(360.0);

  for (int frameCnt = 1; frameCnt < frameMax; ++frameCnt) {

    background(0.0, 0.0, 90.0, 100);
    noFill();
    strokeWeight(1.0);

    float easeRatio  = easeInOutCubic(map(frameCnt, 0, frameMax, 0.0, 1.0));
    int   plotMax    = floor(10000 * easeRatio);
    drawVectorfield(
                    baseHue,
                    5.0,
                    plotMax,
                    0.004
                    );
    drawVectorfield(
                    baseHue + 30.0,
                    20.0,
                    plotMax,
                    0.002
                    );

    casing((baseHue + 0.0) % 360.0);
    saveFrame("frames/" + String.format("%04d", frameCnt) + ".png");

  }

  // to show the last frame in long time
  for (int frameCnt = frameMax; frameCnt < frameMax + lastFrameCnt; ++frameCnt) {
    saveFrame("frames/" + String.format("%04d", frameCnt) + ".png");
  }

  // for twitter thumbnail
  saveFrame("frames/0000.png");
  
  exit();
  
}

/**
 * drawVectorfield    : draw vector field
 * @param  _baseHue   : square color.
 * @param  _alpha     : square alpha.
 * @param  _plotMax   : how many times to draw stroke.
 * @param  _plotScale : vector field step.
 */
private void drawVectorfield(float _baseHue, float _alpha, int _plotMax, float _plotScale) {

  int squareCnt = 0;
  for (float xInit = 0.15; xInit <= 0.9; xInit += 0.05) {
    for (float yInit = 0.15; yInit <= 0.9; yInit += 0.05) {

      float xPrev = 0.0;
      float yPrev = 0.0;
      // using noise() as an array of random
      float xCurr = xInit + map(noise(xInit * 3.0, yInit * 3.0), 0.0, 1.0, -0.03, 0.03);
      float yCurr = yInit + map(noise(xInit * 7.0, yInit * 7.0), 0.0, 1.0, -0.03, 0.03);

      float hueApply = _baseHue + noise(xInit, yInit) * 60.0;
      if (squareCnt % 2 == 0) {
        hueApply += 30.0;
      }
      squareCnt++;
      
      for (int plotCnt = 0; plotCnt < _plotMax; ++plotCnt) {

        // I don't know why this formula makes square shape...
        xPrev += 0.25 * sin(TWO_PI * xCurr * 10.0);
        yPrev += 0.25 * sin(TWO_PI * yCurr * 10.0);
        xCurr += _plotScale * cos(TWO_PI * xPrev * 10.0);
        yCurr += _plotScale * cos(TWO_PI * yPrev * 10.0);

        stroke(
               hueApply % 360.0,
               map(plotCnt, 0, _plotMax, 80.0, 30.0),
               map(plotCnt, 0, _plotMax, 30.0, 60.0),
               _alpha
               );
        point(xCurr * width, yCurr * height);

      }
    }
  }

}

/**
 * casing : draw fancy casing
 * @param  _baseHue : casing color.
 */
private void casing(float _baseHue) {

  fill(0.0, 0.0, 0.0, 0.0);
  strokeWeight(54.0);
  stroke((_baseHue + 30.0) % 360.0, 80.0, 20.0, 100.0);
  rect(0.0, 0.0, width, height);
  strokeWeight(50.0);
  stroke(0.0, 0.0, 100.0, 100.0);
  rect(0.0, 0.0, width, height);
  noStroke();
  noFill();

}

/**
 * easeInOutCubic easing function.
 * @param  _t    0.0 - 1.0 : linear value.
 * @return float 0.0 - 1.0 : eased value.
 */
private float easeInOutCubic(float _t) {
  _t *= 2.0;
  if (_t < 1.0) {
    return pow(_t, 3) / 2.0;
  }
  _t -= 2.0;
  return (pow(_t, 3) + 2.0) / 2.0;
}
